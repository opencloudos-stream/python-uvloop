%global modname uvloop

Summary:        Ultra fast implementation of asyncio event loop on top of libuv
Name:           python-%{modname}
Version:        0.17.0
Release:        8%{?dist}
License:        MIT or ASL 2.0
URL:            https://github.com/MagicStack/uvloop
Source0:        %{url}/archive/v%{version}/%{modname}-%{version}.tar.gz

# https://github.com/MagicStack/uvloop/pull/559
Patch5000:      fix-deadlock-in-testcase.patch

BuildRequires:  gcc
BuildRequires:  libuv-devel

%description
uvloop is a fast, drop-in replacement of the built-in asyncio event loop. uvloop is implemented 
in Cython and uses libuv under the hood.

%package -n python3-%{modname}
Summary:        %{summary}
BuildRequires:  python3-devel python3-setuptools python3-Cython
BuildRequires:  python3-aiohttp python3-psutil python3-pyOpenSSL

%description -n python3-%{modname}
uvloop is a fast, drop-in replacement of the built-in asyncio event loop. uvloop is implemented 
in Cython and uses libuv under the hood.

%prep
%autosetup -p1 -n %{modname}-%{version}

# generate code through cython
sed -i -e "/self.cython_always/s/False/True/" setup.py
# use system libuv
sed -i -e "/self.use_system_libuv/s/False/True/" setup.py
# remove 3rd-party stuff
rm -vrf vendor/

%build
%py3_build

%install
%py3_install

rm -vf %{buildroot}%{python3_sitearch}/%{modname}/_testbase.py
rm -vf %{buildroot}%{python3_sitearch}/%{modname}/__pycache__/_testbase.*

%check
# fix the path of test_libuv_api.py
sed -i "s:import sys:import sys\nsys.path.append\(os.path.abspath\(os.path.dirname\(__file__\)\)\)\n:" tests/__main__.py

# actually the tests are very flaky, thus continue even if they fail
# more information: https://github.com/MagicStack/uvloop/issues/529
%ifnarch ppc64le
%{__python3} setup.py test || :
%endif

%files -n python3-%{modname}
%license LICENSE-APACHE LICENSE-MIT
%doc README.rst
%{python3_sitearch}/%{modname}-*.egg-info/
%{python3_sitearch}/%{modname}/

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.17.0-8
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.17.0-7
- Rebuilt for loongarch release

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.17.0-6
- Rebuilt for python 3.11

* Mon Sep 18 2023 Miaojun Dong <zoedong@tencent.com> - 0.17.0-5
- Rebuild for pyOpenSSL-23.2.0

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.17.0-4
- Rebuilt for OpenCloudOS Stream 23.09

* Mon Sep 04 2023 cunshunxia <cunshunxia@tencent.com> - 0.17.0-3
- fix deadlock in testcase

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.17.0-2
- Rebuilt for OpenCloudOS Stream 23.05

* Thu Apr 20 2023 Wang Guodong <gordonwwang@tencent.com> - 0.17.0-1
- initial build
